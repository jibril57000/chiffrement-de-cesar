gen={}
for first in range(26):
    for second in range(26):
        gen[chr(first+97)+chr(second+97)] = 0
alphabet = [chr(i) for i in range(97,97+26)]
proba = {}
total = 0
with open('La_pdc.txt', 'r') as f:
    for ligne in f.readlines():
        ligne = [car for car in ligne.lower()]
        for i in range(len(ligne)-1):
            total +=1
            enchainement = ligne[i]+ligne[i+1]
            if enchainement[0] in alphabet and enchainement[1] in alphabet:
                gen[ligne[i]+ligne[i+1]]+=1

for key,value in gen.items():
    proba[key] = (value/total)*100

