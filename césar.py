from enchainemt import proba
alphabet={i-97: chr(i) for i in range(97,97+26)}

class César():
    def __init__(self,alphabet):
        self.alphabet = alphabet
    def chiffrer(self,message,k):
        return "".join(self.alphabet[((ord(lettre)-96)+(k+1))%26] for lettre in message)
    def déchiffrer(self,message,k):
        return "".join(self.alphabet[((ord(lettre)-96)-(k+1))%26] for lettre in message)
    def bruteforce(self,message):
        print(f"Calcul de toutes les possibilitées de chiffrement de César sur {message}")
        possible = ["".join(self.alphabet[((ord(lettre) - 96) - (i + 1)) % 26]for lettre in message)for i in range(1, 27)]
        probabilité = {chaine:plausible(chaine) for chaine in possible}
        maximum,cle = 0,""
        for key,values in probabilité.items():
            if values>maximum:
                maximum = values
                cle = key
        print(f"La possibilité la plus probable est {cle}")
        

def plausible(chaine):
    prob = [proba[chaine[i]+chaine[i+1]] for i in range(len(chaine)-1)]
    return sum(prob)/len(prob)

c = César( alphabet)
chiffrement = c.chiffrer("salutjesuisunephraseunpeupluslongue",65)
print(chiffrement)
c.bruteforce(chiffrement)